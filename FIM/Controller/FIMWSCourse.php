<?php
      
	header('Content-type: application/json');

    include '../Class/JsonResponseBean.php';
    include '../Util/FimConstant.php';

    include '../Handler/CourseHandler.php';
    include '../Class/FimCourseClass.php';
    
    
    ///Get Course LIST
    
    echo $_GET["method"];
    
    if($_GET["method"]=="getCourseList"){
        
        $courseList = getCourseList();
    
        if($courseList!=const_error_code){
        	$jsonResponseBean = new JsonResponseBean();
        	$jsonResponseBean->setResponseCode(const_success_code);
        	$jsonResponseBean->setResponseMessage(const_success_message);
        	$jsonResponseBean->setPayload($courseList->getItem());
        	
        	echo json_encode($jsonResponseBean);
        }else{
        	 
        	$jsonResponseBean = new JsonResponseBean();
        	$jsonResponseBean->setResponseCode(const_error_code);
        	$jsonResponseBean->setResponseMessage(const_error_message);
        	echo json_encode($jsonResponseBean);
        }
    
    }else if($_GET["method"]=="getCourseListByID"){
        $id="C001";
        
        if(!empty($id)){
             $courseList = getCourseListByID($id);
            print_r($courseList->getItem());
             
        	$jsonResponseBean = new JsonResponseBean();
        	$jsonResponseBean->setResponseCode(const_success_code);
        	$jsonResponseBean->setResponseMessage(const_success_message);
        	$jsonResponseBean->setPayload($courseList->getItem());
        	
        	echo json_encode($jsonResponseBean);
            
        }else{
        	 
        	$jsonResponseBean = new JsonResponseBean();
        	$jsonResponseBean->setResponseCode(const_error_code);
        	$jsonResponseBean->setResponseMessage(const_error_message);
        	echo json_encode($jsonResponseBean);
        }
        
        
    }else if($_GET["method"]="updateCourseByID"){
        $id="C001";
        
        
        $courseObj = new FIMCourseClass();
        $courseObj->setID($id);
        $courseObj->setCourseName("Food Technology");
        $courseObj->setCourseType("Degree");
        $courseObj->setCourseDesc("New Course");
        
        $isSuccess = updateCourseDetail($courseObj);
        
        if($isSuccess!=const_error_code){
        	
        	$adminProfile = getCourseListByID($courseObj->getID());
        	
        	$jsonResponseBean = new JsonResponseBean();
        	$jsonResponseBean->setResponseCode(const_success_code);
        	$jsonResponseBean->setResponseMessage(const_success_message);
        	$jsonResponseBean->setPayload($adminProfile->getItem());
        	 
        
        	echo json_encode($jsonResponseBean);
        }else{
        
        	$jsonResponseBean = new JsonResponseBean();
        	$jsonResponseBean->setResponseCode(const_error_code);
        	$jsonResponseBean->setResponseMessage(const_error_message);
        	echo json_encode($jsonResponseBean);
        }
        
    }
    else{
        	 
        	$jsonResponseBean = new JsonResponseBean();
        	$jsonResponseBean->setResponseCode(const_error_code);
        	$jsonResponseBean->setResponseMessage(const_error_message);
        	echo json_encode($jsonResponseBean);
    }
    
   
    
?>