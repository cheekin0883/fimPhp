<?php
      

	header('Content-type: application/json');

    include '../Class/JsonResponseBean.php';
    include '../Util/FimConstant.php';

    include '../Handler/SampleHandler.php';
    include '../Class/FimAdminProfileClass.php';
    include '../Class/FimAdminClass.php';

    
    
	/*
	 Get and Process Requested Param Value
     $name = $_POST['username'];
     $password = $_POST['password'];
	 */

    echo $_GET["method"];


    /////////////////////////////////////
    // CREATE ADMIN PROFILE
    ////////////////////////////////////
    if($_GET["method"]=="createAdmin"){
    
        $todayDt = (new DateTime())->format('Y-m-d H:i:s');
        
        echo $todayDt;
        
        $fimAdminProfile  = new FimAdminProfileClass();
        $fimAdminProfile->setName("JEFFREY - ".$todayDt);
        $fimAdminProfile->setIC("999999-10-1010");
        $fimAdminProfile->setGender("MALE");
        $fimAdminProfile->setEmail("jeff@hotmail.com");
        $fimAdminProfile->setAddress("22 JLN 1/34");
        $fimAdminProfile->setCreateDate($todayDt);
        $fimAdminProfile->setStatus("ACTIVE");
        
        $fimAdminClass = new FimAdminClass();
        $fimAdminClass->setAdminID("jeffUser100");
        $fimAdminClass->setAdminPass("abcdefg");
        $fimAdminClass->setAdminLevel("2");
        
        $fimAdminProfile->setFimAdminClass($fimAdminClass);
        
        
        $isSuccess = createAdminProfile($fimAdminProfile);
        
        
        if($isSuccess!=const_error_code){
        $jsonResponseBean = new JsonResponseBean();
        $jsonResponseBean->setResponseCode(const_success_code);
        $jsonResponseBean->setResponseMessage(const_success_message);

        
        
        echo json_encode($jsonResponseBean);
        }else{
        
        $jsonResponseBean = new JsonResponseBean();
        $jsonResponseBean->setResponseCode(const_error_code);
        $jsonResponseBean->setResponseMessage(const_error_message);
        echo json_encode($jsonResponseBean);
        }
    }

    

    /////////////////////////////////////
    // GET ADMIN LIST
    ////////////////////////////////////
    
    if($_GET["method"]=="getAdminList"){
    
        
        $adminList = getAdminList();
    // print_r($adminList->getItem());

        if($adminList!=const_error_code){
            $jsonResponseBean = new JsonResponseBean();
            $jsonResponseBean->setResponseCode(const_success_code);
            $jsonResponseBean->setResponseMessage(const_success_message);
            $jsonResponseBean->setPayload($adminList->getItem());
            
            
        echo json_encode($jsonResponseBean);
        }else{
            
            $jsonResponseBean = new JsonResponseBean();
            $jsonResponseBean->setResponseCode(const_error_code);
            $jsonResponseBean->setResponseMessage(const_error_message);
            echo json_encode($jsonResponseBean);
        }
    
    }
    
    /////////////////////////////////////
    // UPDATE ADMIN PROFILE
    ////////////////////////////////////
   
    /*
    $adminProfileObj = new FimAdminProfileClass();
    $adminProfileObj->setID("1");
    $adminProfileObj->setName("MyXX NAME333");
    $adminProfileObj->setStatus("ACTIVE");
    $adminProfileObj->setEmail("myNewemail@gmail.com");
    
    
    $isSuccess = updateAdminProfile($adminProfileObj);
    //print_r($updatedProfile->getItem());
    
    if($isSuccess!=const_error_code){
    	
    	$adminProfile = getAdminByID($adminProfileObj->getID());
    	
    	$jsonResponseBean = new JsonResponseBean();
    	$jsonResponseBean->setResponseCode(const_success_code);
    	$jsonResponseBean->setResponseMessage(const_success_message);
    	$jsonResponseBean->setPayload($adminProfile->getItem());
    	 
    
    	echo json_encode($jsonResponseBean);
    }else{
    
    	$jsonResponseBean = new JsonResponseBean();
    	$jsonResponseBean->setResponseCode(const_error_code);
    	$jsonResponseBean->setResponseMessage(const_error_message);
    	echo json_encode($jsonResponseBean);
    }
    */
    
    
    
   

    /////////////////////////////////////
    // GET ADMIN BY ID
    ////////////////////////////////////
    
    /*
    
    $adminID = "1";
    $adminProfile = getAdminByID($adminID);
   
    
    foreach ($adminProfile->getItem() as $value) {
    	echo $value->getName()." ";
    	echo $value->getIC();
    	
    }

     
    try{
    	
    	if($adminProfile!=const_error_code){
    		$jsonResponseBean = new JsonResponseBean();
    		$jsonResponseBean->setResponseCode(const_success_code);
    		$jsonResponseBean->setResponseMessage(const_success_message);
    		$jsonResponseBean->setPayload($adminProfile->getItem());
    
    		echo json_encode($jsonResponseBean);
    	}else{
    	
    		$jsonResponseBean = new JsonResponseBean();
    		$jsonResponseBean->setResponseCode(const_error_code);
    		$jsonResponseBean->setResponseMessage(const_error_message);
    		echo json_encode($jsonResponseBean);
    	}
    	
    	
    }catch(Exception $ex){
    	echo $ex->getMessage();
    }
    
  
	*/

    
?>


