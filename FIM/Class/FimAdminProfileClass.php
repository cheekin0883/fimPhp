<?php

class FimAdminProfileClass{

	public $id;
	public $name;
	public $ic;
	public $gender;
	public $email;
	public $address;
	public $createDate;
	public $status;
	public $fimAdminClass;


	public function __construct(){

	}
	
	public function setID($id){
		$this->id = $id;
	}
	
	public function getID(){
		return $this->id;
	}


	public function setName($name){
		$this->name = $name;
	}
	
	public function getName(){
		return $this->name;
	}


	public function setIC($ic){
		$this->ic = $ic;
	}
	
	public function getIC(){
		return $this->ic;
	}


	public function setGender($gender){
		$this->gender = $gender;
	}
	
	public function getGender(){
		return $this->gender;
	}


	public function setEmail($email){
		$this->email = $email;
	}
	
	public function getEmail(){
		return $this->email;
	}


	public function setAddress($address){
		$this->address = $address;
	}
	
	public function getAddress(){
		return $this->address;
	}


	public function setCreateDate($createDate){
		$this->createDate = $createDate;
	}
	
	public function getCreateDate(){
		return $this->createDate;
	}


	function setStatus($status){
		$this->status = $status;
	}
	
	function getStatus(){
		return $this->status;
	}
	
	
	public function setFimAdminClass(FimAdminClass $fimAdminClass){
		$this->fimAdminClass = $fimAdminClass;
	}
	
	public function getFimAdminClass(){
		return $this->fimAdminClass;
	}
	


}
?>