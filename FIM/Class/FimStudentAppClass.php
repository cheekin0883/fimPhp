<?php

class FimNewsClass {

    private $id;
    private $courseApplied;
    private $registrationDate;
    private $processBy;
    private $status;
    private $studentProfileID;
    
    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getCourseApplied() {
        return $this->courseApplied;
    }

    public function getRegistrationDate() {
        return $this->registrationDate;
    }

    public function getProcessBy() {
        return $this->processBy;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getStudentProfileID() {
        return $this->studentProfileID;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setCourseApplied($courseApplied) {
        $this->courseApplied = $courseApplied;
    }

    public function setRegistrationDate($registrationDate) {
        $this->registrationDate = $registrationDate;
    }

    public function setProcessBy($processBy) {
        $this->processBy = $processBy;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function setStudentProfileID($studentProfileID) {
        $this->studentProfileID = $studentProfileID;
    }

}
?>