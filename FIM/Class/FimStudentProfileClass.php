<?php

class FimNewsClass {
    private $id;
    private $name;
    private $ic;
    private $email;
    private $gender;
    private $address;
    private $postcode;
    private $phoneNo;
    private $nationality;
    private $religion;
    private $race;
    private $sourceFrom;
    private $spmResult;
    private $school;
    private $year;
    private $guardianName;
    private $guardianIC;
    private $guardianRelationship;
    
    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getIc() {
        return $this->ic;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getGender() {
        return $this->gender;
    }

    public function getAddress() {
        return $this->address;
    }

    public function getPostcode() {
        return $this->postcode;
    }

    public function getPhoneNo() {
        return $this->phoneNo;
    }

    public function getNationality() {
        return $this->nationality;
    }

    public function getReligion() {
        return $this->religion;
    }

    public function getRace() {
        return $this->race;
    }

    public function getSourceFrom() {
        return $this->sourceFrom;
    }

    public function getSpmResult() {
        return $this->spmResult;
    }

    public function getSchool() {
        return $this->school;
    }

    public function getYear() {
        return $this->year;
    }

    public function getGuardianName() {
        return $this->guardianName;
    }

    public function getGuardianIC() {
        return $this->guardianIC;
    }

    public function getGuardianRelationship() {
        return $this->guardianRelationship;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setIc($ic) {
        $this->ic = $ic;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setGender($gender) {
        $this->gender = $gender;
    }

    public function setAddress($address) {
        $this->address = $address;
    }

    public function setPostcode($postcode) {
        $this->postcode = $postcode;
    }

   public  function setPhoneNo($phoneNo) {
        $this->phoneNo = $phoneNo;
    }

    public function setNationality($nationality) {
        $this->nationality = $nationality;
    }

    public function setReligion($religion) {
        $this->religion = $religion;
    }

    public function setRace($race) {
        $this->race = $race;
    }

    public function setSourceFrom($sourceFrom) {
        $this->sourceFrom = $sourceFrom;
    }

    public function setSpmResult($spmResult) {
        $this->spmResult = $spmResult;
    }

    public function setSchool($school) {
        $this->school = $school;
    }

    public function setYear($year) {
        $this->year = $year;
    }

    public function setGuardianName($guardianName) {
        $this->guardianName = $guardianName;
    }

    public function setGuardianIC($guardianIC) {
        $this->guardianIC = $guardianIC;
    }

    public function setGuardianRelationship($guardianRelationship) {
        $this->guardianRelationship = $guardianRelationship;
    }

}

?>
