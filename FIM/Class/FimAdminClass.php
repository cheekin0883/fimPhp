<?php
class FimAdminClass{

	public $id;
	public $adminID;
	public $adminPass;
	public $adminLevel;
	public $adminProfileID;

	public function __construct(){
	    
	}
	
	public function setID($id){
		$this->id = $id;
	}
	
	public function getID(){
		return $this->id;
	}


	public function setAdminID($adminID){
		$this->adminID = $adminID;
	}


	public function getAdminID(){
		return $this->adminID;
	}


	public function setAdminPass($adminPass){
		$this->adminPass = $adminPass;
	}


	function getAdminPass(){
		return $this->adminPass;
	}


	function setAdminLevel($adminLevel){
		$this->adminLevel = $adminLevel;
	}


	function getAdminLevel(){
		return $this->adminLevel;
	}




	function setAdminProfileID($adminProfileID){
		$this->adminProfileID = $adminProfileID;
	}


	function getAdminProfileID(){
		return $this->adminProfileID;
	}
}
?>