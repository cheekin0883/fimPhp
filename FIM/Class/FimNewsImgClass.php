<?php

class FimNewsImgClass {
    private $id;
    private $image;
    private $fimNewsID;
    
    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getImage() {
        return $this->image;
    }

   public function getFimNewsID() {
        return $this->fimNewsID;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setImage($image) {
        $this->image = $image;
    }

    public function setFimNewsID($fimNewsID) {
        $this->fimNewsID = $fimNewsID;
    }

}
?>