<?php

class FimCourseClass {
    
    public $id;
    public $courseID;
    public $courseName;
    public $courseType;
    public $courseDesc;
    public $courseMQACode;
    public $courseSubjectList;
    public $courseprice;
    public $courseSchedule;
    public $courseBronchure;

    function __construct() {
        
    }
    
   public function getId() {
        return $this->id;
    }

    public function getCourseID() {
        return $this->courseID;
    }

    public function getCourseName() {
        return $this->courseName;
    }

    public function getCourseType() {
        return $this->courseType;
    }

    public function getCourseDesc() {
        return $this->courseDesc;
    }

    public function getCourseMQACode() {
        return $this->courseMQACode;
    }

    public function getCourseSubjectList() {
        return $this->courseSubjectList;
    }

   public  function getCourseprice() {
        return $this->courseprice;
    }

    public function getCourseSchedule() {
        return $this->courseSchedule;
    }

     public function getCourseBronchure() {
        return $this->courseBronchure;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setCourseID($courseID) {
        $this->courseID = $courseID;
    }

    public function setCourseName($courseName) {
        $this->courseName = $courseName;
    }

    public function setCourseType($courseType) {
        $this->courseType = $courseType;
    }

    public function setCourseDesc($courseDesc) {
        $this->courseDesc = $courseDesc;
    }

    public function setCourseMQACode($courseMQACode) {
        $this->courseMQACode = $courseMQACode;
    }

    public function setCourseSubjectList($courseSubjectList) {
        $this->courseSubjectList = $courseSubjectList;
    }

    public function setCourseprice($courseprice) {
        $this->courseprice = $courseprice;
    }

    public function setCourseSchedule($courseSchedule) {
        $this->courseSchedule = $courseSchedule;
    }

    public function setCourseBronchure($courseBronchure) {
        $this->courseBronchure = $courseBronchure;
    }
}
?>