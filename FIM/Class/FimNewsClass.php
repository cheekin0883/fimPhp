<?php

class FimNewsClass {

    private $id;
    private $title;
    private $description;
    private $dateTimeFrom;
    private $dateTimeto;
    private $lastUpdateBy;
    
    function __construct() {
       
    }
    
   
    public function getId() {
        return $this->id;
    }

    public function getTitle() {
        return $this->title;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getDateTimeFrom() {
        return $this->dateTimeFrom;
    }

    public function getDateTimeto() {
        return $this->dateTimeto;
    }

    public function getLastUpdateBy() {
        return $this->lastUpdateBy;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setDateTimeFrom($dateTimeFrom) {
        $this->dateTimeFrom = $dateTimeFrom;
    }

    public function setDateTimeto($dateTimeto) {
        $this->dateTimeto = $dateTimeto;
    }

    public function setLastUpdateBy($lastUpdateBy) {
        $this->lastUpdateBy = $lastUpdateBy;
    }



}

?>