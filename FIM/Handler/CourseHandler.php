<?php
    
    function getCourseList() {
    
    	require '../Util/DBConnection.php';
    	require '../Util/Collection.php';
    	
    	try{
    		
    		$stmt =  $conn->prepare("select * from FIM_COURSE;");
    		$stmt->execute();
    				
        	$courseList = new Collection ();
    	
    		while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
    							
    			$courseObj = new FimCourseClass();
    			$courseObj->setId( $row["ID"] );
    			$courseObj->setCourseId( $row["COURSE_ID"] );
    			$courseObj->setCourseName( $row["COURSE_NAME"]);
    			$courseObj->setCourseType( $row["COURSE_TYPE"]);
    			$courseObj->setCourseDesc( $row["COURSE_DESC"]);
    			$courseObj->setCourseMQACode( $row["COURSE_MQA_CODE"]);
    			$courseObj->setCourseSubjectList( $row["COURSE_SUBJECT_LIST"]);
    			$courseObj->setCoursePrice( $row["COURSE_PRICE"]);
    			$courseObj->setCourseSchedule( $row["COURSE_SCHEDULE"]);
    			$courseObj->setCourseBronchure( $row["COURSE_BRONCHURE"]);
    			$courseList->addItem($courseObj);
    		}
   		
    		
    	}catch(Exception $ex){
    		echo $ex->getMessage();
    		return const_error_code;
    	}
    
    	$stmt = null;
    	$conn = null;
    		
    	return $courseList;
    }
    
    
    
    
    function getCourseListById($courseId) {
    
    	require '../Util/DBConnection.php';
    	require '../Util/Collection.php';
    	
    	try{
    		
    		$stmt =  $conn->prepare("select * from FIM_COURSE where COURSE_ID=:courseID;");
    		$stmt->bindParam(':courseID', $courseId);
    		$stmt->execute();
    				
    		$courseList = new Collection ();
    		
    	
    		while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
    							
    			$courseObj = new FimCourseClass();
    			$courseObj->setId( $row["ID"] );
    			$courseObj->setCourseId( $row["COURSE_ID"] );
    			$courseObj->setCourseName( $row["COURSE_NAME"]);
    			$courseObj->setCourseType( $row["COURSE_TYPE"]);
    			$courseObj->setCourseDesc( $row["COURSE_DESC"]);
    			$courseObj->setCourseSubjectList( $row["COURSE_SUBJECT_LIST"]);
    			$courseObj->setCoursePrice( $row["COURSE_PRICE"]);
    			$courseObj->setCourseSchedule( $row["COURSE_SCHEDULE"]);
    			$courseObj->setCourseBronchure( $row["COURSE_BRONCHURE"]);
    			$courseList->addItem ( $courseObj );
    		
    		}
    		
    	}catch(Exception $ex){
    		echo $ex->getMessage();
    		return const_error_code;
    	}
    
    	$stmt = null;
    	$conn = null;
    		
    	return $courseList;
    }
    
    
function updateCourseDetail(FIMCourseClass $fimCourseObj) {

	require '../Util/DBConnection.php';
	
	try{
		
		$sql = "UPDATE FIM_COURSE SET ID=ID ";
		$sqlUptColumn = "";
		$sqlWhere = "WHERE 1=1 ";
		
	
		if($fimCourseObj->getCourseName()!=null){
			$sqlUptColumn .=",COURSE_NAME = :courseName ";
		}
		if($fimCourseObj->getCourseType()!=null){
			$sqlUptColumn .=",COURSE_TYPE = :courseType ";
		}
		if($fimCourseObj->getCourseDesc()!=null){
			$sqlUptColumn .=",COURSE_DESC= :COURSE_DESC ";
		}
		
		if($fimCourseObj->getCourseId()!=null){
			$sqlWhere .="AND COURSE_ID = :courseID ";
		}
	
		$stmt =  $conn->prepare($sql.$sqlUptColumn.$sqlWhere);
		
	
		if($fimCourseObj->getCourseName()!=null){
		    $courseName = $fimCourseObj->getCourseName();
			$stmt->bindParam(':courseName', $courseName);	
		}
		if($fimCourseObj->getCourseType()!=null){
		    $courseType = $fimCourseObj->getCourseType();
			$stmt->bindParam(':courseType', $courseType);		
		}
		if($fimCourseObj->getCourseDesc()!=null){
		    $courseDesc = $fimCourseObj->getCourseDesc();
			$stmt->bindParam(':COURSE_DESC', $courseDesc);
		}
		if($fimCourseObj->getCourseId()!=null){
		    $courseID = $fimCourseObj->getCourseId();
			$stmt->bindParam(':courseID', $fimCourseObj->$courseID);
		}
		
		$stmt->execute();


	}catch(Exception $ex){
		echo $ex->getMessage();
		return const_error_code;
	}

	$stmt = null;
	$conn = null;
	
	return const_success_code;
}


    
    
    
?>