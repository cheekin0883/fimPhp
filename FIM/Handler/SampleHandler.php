<?php


function getAdminList() {

	require '../Util/DBConnection.php';
	require '../Util/Collection.php';
	
	try{
		
		$stmt =  $conn->prepare("select * from FIM_ADMIN_PROFILE fap, FIM_ADMIN fa where fap.ID = fa.ADMIN_PROFILE_ID ;");
		$stmt->execute();
				
		$adminProfileList = new Collection ();
		
	
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
							
			$fimProfileObj = new FimAdminProfileClass ();
			$fimProfileObj->setID ( $row ["ID"] );
			$fimProfileObj->setName ( $row ["NAME"] );
			$fimProfileObj->setIC ( $row ["IC"] );
			$fimProfileObj->setGender ( $row ["GENDER"] );
			$fimProfileObj->setEmail ( $row ["EMAIL"] );
			$fimProfileObj->setAddress ( $row ["ADDRESS"] );
			$fimProfileObj->setCreateDate ( $row ["CREATE_DATE"] );
			$fimProfileObj->setStatus ( $row ["STATUS"] );
				
			$fimAdminClassObj = new FimAdminClass ();
			$fimAdminClassObj->setID($row["ID"]);
			$fimAdminClassObj->setAdminID ( $row ["ADMIN_ID"] );
			$fimAdminClassObj->setAdminLevel ( $row ["ADMIN_LEVEL"] );
			$fimAdminClassObj->setAdminPass ( $row ["ADMIN_PASS"] );
			$fimAdminClassObj->setAdminProfileID($row["ADMIN_PROFILE_ID"]);
		
			
		    $fimProfileObj->setFimAdminClass($fimAdminClassObj);				
			$adminProfileList->addItem ( $fimProfileObj );
		
		}
		
		
	}catch(Exception $ex){
		echo $ex->getMessage();
		return const_error_code;
	}

	$stmt = null;
	$conn = null;
		
	return $adminProfileList;
}



function getAdminByID($adminID){

 	require "../Util/DBConnection.php";
	

	try{
		
		$stmt = $conn->prepare("select * from FIM_ADMIN_PROFILE fap, FIM_ADMIN fa where fap.ID = fa.ADMIN_PROFILE_ID and fap.ID = :fapId ;");
		$stmt->bindParam(':fapId', $adminID);
		$stmt->execute();
		        
	
		$adminProfileList = new Collection();
		

		
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
			
		
			$fimProfileObj = new FimAdminProfileClass();
			$fimProfileObj->setID ( $row ["ID"] );
			
			$fimProfileObj->setName ( $row ["NAME"] );
			$fimProfileObj->setIC ( $row ["IC"] );
			$fimProfileObj->setGender ( $row ["GENDER"] );
			$fimProfileObj->setEmail ( $row ["EMAIL"] );
			$fimProfileObj->setAddress ( $row ["ADDRESS"] );
			$fimProfileObj->setCreateDate ( $row ["CREATE_DATE"] );
			$fimProfileObj->setStatus ( $row ["STATUS"] );
				
		
			$fimAdminClassObj = new FimAdminClass ();
			$fimAdminClassObj->setID($row["ID"]);
			$fimAdminClassObj->setAdminID ( $row ["ADMIN_ID"] );
			$fimAdminClassObj->setAdminLevel ( $row ["ADMIN_LEVEL"] );
			$fimAdminClassObj->setAdminPass ( $row ["ADMIN_PASS"] );
			$fimAdminClassObj->setAdminProfileID($row["ADMIN_PROFILE_ID"]);

		    $fimProfileObj->setFimAdminClass($fimAdminClassObj);				
			$adminProfileList->addItem ( $fimProfileObj );
	

	    }

    	$stmt = null;
		$conn = null;
			
			
	    return $adminProfileList;
		
	}catch(Exception $ex){
		echo $ex->getMessage();
		return const_error_code;
	}
	



}

function createAdminProfile(FimAdminProfileClass $fimAdminProfileClass) {
	require '../Util/DBConnection.php';
	
	try {


		$stmt = $conn->prepare ( "INSERT INTO FIM_ADMIN_PROFILE (NAME,IC,GENDER,EMAIL,ADDRESS,CREATE_DATE,STATUS) VALUES (:name,:ic,:gender,:email,:address,:createDt,:status)" );
		
	    $todayDt = (new DateTime())->format('Y-m-d H:i:s');
		
		$stmt->bindParam ( ':name', $fimAdminProfileClass->name);
		$stmt->bindParam ( ':ic', $fimAdminProfileClass->ic );
		$stmt->bindParam ( ':gender', $fimAdminProfileClass->gender );
		$stmt->bindParam ( ':email', $fimAdminProfileClass->email );
		$stmt->bindParam ( ':address', $fimAdminProfileClass->address );
		$stmt->bindParam ( ':createDt', $todayDt);
		$stmt->bindParam ( ':status', $fimAdminProfileClass->status );
		
		$conn->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		
		if ($stmt->execute ()) {
			$parentID = $conn->lastInsertId ();
			echo $parentID;
			
			$stmtChild = $conn->prepare ( "INSERT INTO FIM_ADMIN (ADMIN_ID,ADMIN_PASS,ADMIN_LEVEL,ADMIN_PROFILE_ID) VALUES(:adminId,:adminPass,:adminLvl,:adminProfileID)" );
			
			$stmtChild->bindParam ( ':adminId', $fimAdminProfileClass->fimAdminClass->adminID );
			$stmtChild->bindParam ( ':adminPass', $fimAdminProfileClass->fimAdminClass->adminPass );
			$stmtChild->bindParam ( ':adminLvl', $fimAdminProfileClass->fimAdminClass->adminLevel );
			$stmtChild->bindParam ( ':adminProfileID', $parentID );
			
			$conn->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
			
			if ($stmtChild->execute ()) {
				
				$stmt = null;
				$conn = null;
				return const_success_code;
			} else {
				
				$stmt = null;
				$conn = null;
				return const_error_code;
			}
		} else {
			
			$stmt = null;
			$conn = null;
			return const_error_code;
		}
	} catch ( Exception $ex ) {
		echo $ex->getMessage ();
		return const_error_code;
	}
}




function updateAdminProfile(FimAdminProfileClass $adminProfileObj) {

	require '../Util/DBConnection.php';
	
	try{
		
		$sql = "UPDATE FIM_ADMIN_PROFILE fap SET fap.ID=fap.ID ";
		$sqlUptColumn = "";
		$sqlWhere = "WHERE 1=1 ";
		
		if($adminProfileObj->getName()!=null){
			$sqlUptColumn .=",fap.NAME = :fapName ";
		}	
		if($adminProfileObj->getStatus()!=null){
			$sqlUptColumn .=",fap.STATUS = :fapStatus ";
		}
		if($adminProfileObj->getEmail()!=null){
			$sqlUptColumn .=",fap.EMAIL = :fapEmail ";
		}
		if($adminProfileObj->getID()!=null){
			$sqlWhere .="AND fap.ID= :fapID ";
		}
		
	
		$stmt =  $conn->prepare($sql.$sqlUptColumn.$sqlWhere);
		
	
		if($adminProfileObj->getName()!=null){
			$stmt->bindParam(':fapName', $adminProfileObj->name); //use get cuased eror		
		}
		if($adminProfileObj->getStatus()!=null){
			$stmt->bindParam(':fapStatus', $adminProfileObj->status);		
		}
		if($adminProfileObj->getEmail()!=null){
			$stmt->bindParam(':fapEmail', $adminProfileObj->email);
		}
		if($adminProfileObj->getID()!=null){
			$stmt->bindParam(':fapID', $adminProfileObj->id);
		}
		
		
		$stmt->execute();


	}catch(Exception $ex){
		echo $ex->getMessage();
		return const_error_code;
	}

	$stmt = null;
	$conn = null;
	
	return const_success_code;
}








?>